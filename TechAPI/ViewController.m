//
//  ViewController.m
//  TechAPI
//
//  Created by 田中 賢治 on 2014/01/16.
//  Copyright (c) 2014年 com.ktanaka. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //API取得
    NSURL *url = [NSURL URLWithString:@"http://api.openweathermap.org/data/2.5/weather?q=Sendai-shi,jp&mode=json"];
    NSURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection connectionWithRequest:request delegate:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



// データ受信時に１回だけ呼び出される。
- (void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    //初期化
    self.mData = [NSMutableData data];
}

// データ受信したら何度も呼び出されるメソッド。
- (void) connection:(NSURLConnection *) connection didReceiveData:(NSData *)data
{
    //通信したデータを入れていきます
    [self.mData appendData:data];
}

// データ受信が終わったら呼び出されるメソッド。
- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error = nil;
    
    //NSDictinaryがNSArrayになって返ってきます(id の中に入る)
    id json = [NSJSONSerialization JSONObjectWithData:self.mData options:NSJSONReadingAllowFragments error:&error];
    
    if(!error){
        
        NSLog(@"%@",json);
        
    }
}

//通信エラー時に呼ばれる
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //エラー処理を書く
    
}

@end
