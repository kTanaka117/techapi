//
//  ViewController.h
//  TechAPI
//
//  Created by 田中 賢治 on 2014/01/16.
//  Copyright (c) 2014年 com.ktanaka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<NSURLConnectionDataDelegate>

@property NSMutableData *mData;

@end
